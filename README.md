# ci-test-container

This is a repository configured to automatically build a docker image with the [oss-cad-suite] installed and publish it to GitLab container registry.

The container contains:
  - sbt (for chisel)
  - oss-cad-suite (contains e.g. RTL simulators and yosys)
  - Berkeley TestFloat and SoftFloat
  - RISC-V GCC toolchain for a variety of extension combinations
  - Various python packages needed for running A-Core verification environment

[oss-cad-suite]: https://github.com/YosysHQ/oss-cad-suite-build
