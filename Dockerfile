FROM ubuntu:22.04
ARG DEBIAN_FRONTEND=noninteractive


RUN apt-get update && apt-get install -y \
    git \
    wget \
    curl \
    python3 \
    python3-pip \
    python-is-python3 \
    default-jdk \
    vim \
    iverilog \
    unzip \
    tcsh \
    autoconf \
    automake \
    autotools-dev \
    libmpc-dev \
    libmpfr-dev \
    libgmp-dev \
    gawk \
    build-essential \
    bison \
    flex \
    texinfo \
    gperf \
    libtool \
    patchutils \
    bc \
    zlib1g-dev \
    libexpat-dev \
    ninja-build \
    cmake \
    libglib2.0-dev \
    rsync
    

# Install sbt
RUN apt-get update && \
    apt-get install apt-transport-https curl gnupg -yqq && \
    echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list && \
    echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list && \
    curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | gpg --no-default-keyring --keyring    gnupg-ring:/etc/apt/trusted.gpg.d/scalasbt-release.gpg --import && \
    chmod 644 /etc/apt/trusted.gpg.d/scalasbt-release.gpg && \
    apt-get update && \
    apt-get install sbt

# Install a-core specific oss-cad-suite
RUN wget -q https://gitlab.com/a-core/oss-cad-suite-build/-/package_files/139654461/download
RUN tar -xf download
ENV PATH=$PATH:/oss-cad-suite/bin

# Install TestFloat and SoftFloat
RUN mkdir TestFloat
RUN cd /TestFloat && \
wget "http://www.jhauser.us/arithmetic/SoftFloat-3e.zip" && \
wget "http://www.jhauser.us/arithmetic/TestFloat-3e.zip" && \
unzip -q SoftFloat-3e.zip && \
unzip -q TestFloat-3e.zip
RUN cd /TestFloat/SoftFloat-3e/build/Linux-x86_64-GCC && make
RUN cd /TestFloat/TestFloat-3e/build/Linux-x86_64-GCC && make
RUN rm -rf TestFloat/SoftFloat-3e.zip
RUN rm -rf TestFloat/TestFloat-3e.zip
ENV TESTFLOAT_PATH=/TestFloat/TestFloat-3e/build/Linux-x86_64-GCC/

RUN python3 -m pip install --user \
    wheel \
    numpy \
    numpydoc \
    matplotlib \
    joblib \
    scipy \
    pandas \
    sphinx \
    sphinx_rtd_theme \
    PyQt5 \
    pyelftools \
    sortedcontainers \
    bitstring \
    pyyaml \
    cocotb \
    cocotb_test \
    pytest \
    cocotbext-axi

RUN git clone https://github.com/riscv-collab/riscv-gnu-toolchain.git && \
cd riscv-gnu-toolchain && \
./configure --prefix=/opt/riscv --with-multilib-generator="rv32imf_zicsr-ilp32f--c;rv32im_zicsr-ilp32--c;rv32i_zicsr-ilp32--c;rv32ima_zicsr-ilp32--c;" && \
make
ENV PATH=$PATH:/opt/riscv/bin

